# Тестовое задание для TrueNorth

Сделано на:
- Клиент - Angular 4 (./coffemaker)
- Бэкенд - .NET Core 2.0 WebApi


## Как запускать 

На винде чтобы открыть солюшен нужна Visual Studio 2017 и установленный .NET Core SDK:

https://aka.ms/dotnet-sdk-2.0.0-win-gs-x64

если просто запустить только SDK а дальше 

	dotnet run

ангуляр там все просто - обычный ангуляр
	
	cd coffemaker
	npm install
	npm install -g @angular/cli	
	ng serve

## Как пользоваться

1. нажимаем на зеленые кнопки над номиналами (типа закладываем деньги)
2. нажимаем белую кнопку напротив нужного продукта с нужным размером **L**,**M**,**S**
3. улетает запрос, появляются иконки со сдачей если она есть, сообщение в логе

## "Протокол"

**FC 2324** не совместимый )

Запрос:
	
Название продукта, размер стаканчика, и сумма внесенных средств

	{
    	"name": "Americano",
    	"size": "large",
	    "sum": 150
	}

Ответ: успех

Результат, что нужно налить и из каких кассет выдать сдачу, какой стакан дать

	{
	    "result": "success",
		"message": "",
	    "components": [
	        {"name": "beans", "quantity": 20},
	        {"name": "water", "quantity": 10},
	        {"name": "milk", "quantity": 10},
	        {"name": "whiskey", "quantity": 5},
	    ],
	    change: {
	        { "denomination": 1, "type": "coin", "count": 2},
			{ "denomination": 10, "type": "coin", "count": 2 },
	    }
	    "size": "large"
	}

Сообщения ошибок:

- **This product is more expensive. Add money.** - внесенных средств недостаточно
- **Out of paper cups** - кончились стаканчики
- **No such product** - не такого продукта
- **Unable to dispense change. Out of {denomination} denomination {type}s** - не могу выдать сдачу т.к. закончились деньги в кассете с нужным номиналом
- **Unable to dispense: Out of {component.name}** - закончился нужный ингредиент 

