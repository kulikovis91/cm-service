using Microsoft.AspNetCore.Mvc;
using cm_service.Common;
using cm_service.Logic;
using Microsoft.AspNetCore.Cors;

namespace cm_service.Controllers
{    
    public class ApiController : Controller
    {
        /// <summary>
        /// �� � ������� ��� ��� ����� ������ ����� IoC ����������� 
        /// �� �� �� ������ ���� ��
        /// </summary>
        private Products products = new Products();
        private CoffeMakerLogic coffeMaker = new CoffeMakerLogic();

        [HttpPost]
        [EnableCors("AllowAllOrigins")]
        public IActionResult Brew([FromBody]BrewRequest request)
        {
            return Json(coffeMaker.Brew(request.name, request.size, request.sum));
        }

        [HttpGet]
        [EnableCors("AllowAllOrigins")]
        public IActionResult GetProducts()
        {
            return Json(products.GetAll());
        }
    }
}
