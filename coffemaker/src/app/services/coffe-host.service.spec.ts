import { TestBed, inject } from '@angular/core/testing';

import { CoffeHostService } from './coffe-host.service';

describe('CoffeHostService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoffeHostService]
    });
  });

  it('should be created', inject([CoffeHostService], (service: CoffeHostService) => {
    expect(service).toBeTruthy();
  }));
});
