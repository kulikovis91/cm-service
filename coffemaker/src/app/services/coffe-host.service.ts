import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CoffeHostService {

  constructor(public http:Http) { 
  }

  getProducts() {
    return this.http.get('http://localhost:8080/Api/GetProducts')
      .map(res => res.json());
  }

  postBrewRequest(obj) {    
    return this.http.post("http://localhost:8080/Api/Brew", obj)
      .map(res => res.json());
  }
}
