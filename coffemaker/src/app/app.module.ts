import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CoffemakerComponent } from './components/coffemaker/coffemaker.component';

import { CoffeHostService } from './services/coffe-host.service';

@NgModule({
  declarations: [
    AppComponent,
    CoffemakerComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
    CoffeHostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
