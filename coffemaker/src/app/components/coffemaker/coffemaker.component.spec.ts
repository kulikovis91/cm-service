import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoffemakerComponent } from './coffemaker.component';

describe('CoffemakerComponent', () => {
  let component: CoffemakerComponent;
  let fixture: ComponentFixture<CoffemakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoffemakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoffemakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
