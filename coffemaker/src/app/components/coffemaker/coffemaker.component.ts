import { Component, OnInit } from '@angular/core';
import { CoffeHostService } from '../../services/coffe-host.service';

@Component({
  selector: 'app-coffemaker',
  templateUrl: './coffemaker.component.html',
  styleUrls: ['./coffemaker.component.css']
})
export class CoffemakerComponent implements OnInit {  
  cashInserted = Array<Money>();
  products = Array<Product>();  
  response:Response;
  strResponse:string;

  constructor(private service:CoffeHostService) {     

  }

  ngOnInit() {
    this.cashInserted = [
      {denomination: 1, count: 0, type: "coin"},
      {denomination: 2, count: 0, type: "coin"},
      {denomination: 5, count: 0, type: "coin"},
      {denomination: 10, count: 0, type: "coin"},
      {denomination: 50, count: 0, type: "note"},
      {denomination: 100, count: 0, type: "note"}      
    ]

    this.service.getProducts().subscribe((products) => {
      this.products = products;
    });
  }

  AddCash(denomId) {
    this.cashInserted[denomId].count++;
  }

  PostBrewRequest(obj, size) {    
    var sum = 0;
    this.cashInserted.forEach(denom => {
      sum += denom.count * denom.denomination;
      denom.count = 0;
    });
    
    this.service.postBrewRequest({
        name: obj.name,
        size: size,
        sum: sum
    }).subscribe((res) => {
      this.response = res;      
      this.strResponse = JSON.stringify(res);
    });
  }
}

interface Money {
  denomination:number;
  count: number;
  type:string;
}

interface Product {
    name:string;
    price:number;
}

interface CoffeComponent {
  name:string;
  quantity:number;
}

interface Response {
  result: string;
  message: string;
  size: string;
  components:Component[];
  change:Money[]
}