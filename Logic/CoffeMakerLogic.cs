﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cm_service.Common;

namespace cm_service.Logic
{
    public class CoffeMakerLogic
    {
        private Products products = new Products();

        private List<Component> componentReservoir = new List<Component>()
        {
           new Component { name = "beans",   quantity = 1000},
           new Component { name = "milk",    quantity = 1000},
           new Component { name = "water",   quantity = 1000},
           new Component { name = "whiskey", quantity = 1},
           new Component { name = "sugar",   quantity = 1000}
        };

        private List<CupStack> cupStacks = new List<CupStack>()
        {
            new CupStack { size = "large", count = 100 },
            new CupStack { size = "medium", count = 1000 },
            new CupStack { size = "small", count = 10000 }
        };

        private List<Money> cash = new List<Money>
        {
              new Money {denomination = 1,   count = 0,  type = "coin"},
              new Money {denomination = 2,   count = 5000,  type = "coin"},
              new Money {denomination = 5,   count = 5000,  type = "coin"},
              new Money {denomination = 10,  count = 10000, type = "coin"},
              new Money {denomination = 50,  count = 10000, type = "note"},
              new Money {denomination = 100, count = 10000, type = "note"}
        };

        public BrewResponse Brew(string name, string size, int sum)
        {
            var response = new BrewResponse();
            response.result = false;
            var product = products.Get(name);
            var message = "";
            var change = new List<Money>();
            var components = new List<Component>();

            // нет времени на стейт машину - поэтому каскад ифов
            if (product != null)
            {
                var modPrice = product.price;
                if (size == "small") modPrice /= 2;
                else if (size == "large") modPrice *= 2;
                
                if (modPrice <= sum)
                {
                    if (cupStacks.FirstOrDefault(s => s.size == size).count > 0)
                    {
                        cupStacks.FirstOrDefault(s => s.size == size).count--;
                        if (GetChange(sum - modPrice, out change, out message))
                        {
                            if (Dispense(product, out components, out message))
                            {
                                response.result = true;
                                response.change = change;
                                response.components = components;
                                response.size = size;
                            }
                            else
                                response.message = message;
                        }
                        else
                            response.message = message;
                    }
                    else
                        response.message = "Out of paper cups";
                }
                else
                    response.message = "This product is more expensive. Add money.";
            }
            else
                response.message = "No such product";

            return response;
        }

        private Boolean GetChange(int sum, out List<Money> change, out string message)
        {
            change = new List<Money>();
            message = "";
            var tmpSum = sum;
            var result = true;
            foreach(var c in cash.OrderByDescending(el => el.denomination))
            {
                var div = tmpSum / c.denomination;
                if (div >= 1 && c.count >= div)
                {
                    tmpSum -= c.denomination * div;
                    change.Add(new Money()
                    {
                        count = div,
                        denomination = c.denomination,
                        type = c.type
                    });
                }
                else if (c.count < div)
                {
                    message = $"Unable to dispense change. Out of {c.denomination} denomination {c.type}s";
                    result = false;
                    break;
                }
            }
                
            return result;
        }

        private Boolean Dispense(Product product, out List<Component> components, out string message) 
        {
            components = new List<Component>();
            message = "";
            var result = true;
            foreach(var c in product.recepie)
            {
                if (componentReservoir.FirstOrDefault(cr => cr.name == c.name && cr.quantity >= c.quantity) == null)
                {
                    result = false;
                    message = $"Unable to dispense: Out of {c.name}";
                }
                else
                    components.Add(c);
            };
            
            return result;
        }
    }
   
    public class CupStack
    {
        public string size { get; set; }
        public int count { get; set; }
    }   
}
