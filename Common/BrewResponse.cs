﻿using System.Collections.Generic;

namespace cm_service.Common
{
    public class BrewResponse
    {
        public bool result { get; set; }
        public string message { get; set; }
        public List<Component> components { get; set; }
        public List<Money> change { get; set; }
        public string size { get; set; }
    }
}
