﻿namespace cm_service.Common
{
    public class BrewRequest
    {
        public string name { get; set; }
        public string size { get; set; }
        public int sum { get; set; }
    }
}
