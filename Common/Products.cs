﻿using System.Collections.Generic;
using System.Linq;

namespace cm_service.Common
{
    public class Products
    {
        public List<Product> products = new List<Product>()
        {
            new Product {name = "Latte", price = 18,
                recepie = new List<Component>() {
                    new Component { name = "beans", quantity = 10 },
                    new Component { name = "water", quantity = 10 },
                    new Component { name = "milk", quantity = 10 },
                    new Component { name = "sugar", quantity = 10 },
                }       
            },
            new Product {name = "Capucchino", price = 32,
                recepie = new List<Component>() {
                    new Component { name = "beans", quantity = 10 },
                    new Component { name = "water", quantity = 10 },
                    new Component { name = "milk", quantity = 10 },
                    new Component { name = "sugar", quantity = 10 },
                }
            },
            new Product {name = "Americano", price = 10,
                recepie = new List<Component>() {
                    new Component { name = "beans", quantity = 10 },
                    new Component { name = "water", quantity = 10 },
                    new Component { name = "milk", quantity = 10 },
                    new Component { name = "sugar", quantity = 10 },
                }
            },
            new Product {name = "Espresso", price = 26,
                recepie = new List<Component>() {
                    new Component { name = "beans", quantity = 10 },
                    new Component { name = "water", quantity = 10 },
                    new Component { name = "milk", quantity = 10 },
                    new Component { name = "sugar", quantity = 10 },
                }
            },
            new Product {name = "Russiano", price = 6,
                recepie = new List<Component>() {
                    new Component { name = "beans", quantity = 10 },
                    new Component { name = "water", quantity = 10 },
                    new Component { name = "milk", quantity = 10 },
                    new Component { name = "sugar", quantity = 10 },
                }
            },
            new Product {name = "Irish", price = 40,
                recepie = new List<Component>() {
                    new Component { name = "beans", quantity = 10 },
                    new Component { name = "water", quantity = 10 },
                    new Component { name = "milk", quantity = 10 },
                    new Component { name = "sugar", quantity = 10 },
                    new Component { name = "whiskey", quantity = 10 },
                }
            },
        };

        public Product Get(string name)
        {
            return products.FirstOrDefault(p => p.name == name);
        }

        public List<Product> GetAll()
        {
            return products;
        }
    }

    public class Product
    {
        public string name { get; set; }
        public int price { get; set; }        
        public List<Component> recepie { get; set; }
    }
}
