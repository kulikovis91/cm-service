﻿namespace cm_service.Common
{
    public class Money
    {
        public int denomination { get; set; }
        public string type { get; set; }
        public int count { get; set; }
    }
}
