﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cm_service.Common
{
    public class Component
    {
        public string name { get; set; }
        public int quantity { get; set; }
    }
}
